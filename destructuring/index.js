// alert("Untuk pembagian tugas siswa untuk acara pentas seni di hari sabtu, anda harus menjawab beberapa pertanyaan !");

let name = prompt("Masukkan nama anda :", "Nama...");
let kelas = prompt("Kelas : ", "contoh :(7, 8, atau 9 :"); 
let jenisKelamin = prompt("Jenis Kelamin :", "L/P :")

while(!name || !kelas){
	alert('Semua jawaban harus diisi !');
	name = prompt("Masukkan nama anda :", "Nama...");
	kelas = prompt("Kelas : ", "contoh :(7, 8, atau 9 :");
	jenisKelamin = prompt("Jenis Kelamin :");
};



const tugas = {

	tugasLakiLaki: {
		kelasTujuh: 'Membersihkan area panggung dan mengeset kursi penonton.',
		kelasDelapan: 'Membersihkan dan merapihkan tempat parkir.',
		kelasSembilan: 'Membersihkan backstage dan menyiapkan alat alat yang dibutuhkan.'
	},
	tugasPerempuan: {
		kelasTujuh: 'Menata dan mendekorasi area panggung.',
		kelasDelapan: 'Menyiapkan konsumsi untuk acara.',
		kelasSembilan: 'Mengkoordinasi semua rundown acara dan belanja bahan untuk konsumsi.'
	}
};

//console.log(tugas.tugasLakiLaki.kelasDelapan);
	const jamTugas = ['07:00 - 10:00','09:00 - 12:00','10:00 - 14:00'];
	const [ sesiSatu, sesiDua, sesiTiga ] = jamTugas;

/**
 * @params {kls} dari prompt kelas 
*/

function pembagianJamTugas(kls){
	
	if (kls == 7) { return sesiSatu }
	if (kls == 8) { return sesiDua }
	if (kls == 9) { return sesiTiga }
};


/**
 * @params { lkt, lkd, lks } dari object nested  tugasLakiLaki
 * @params { pkt, pkd ,pks } dari object nested tugaPerempuan 
 * */
const pembagianTugas = ( {tugasLakiLaki: { kelasTujuh:lkt, kelasDelapan:lkd, kelasSembilan:lks },tugasPerempuan: { kelasTujuh:pkt, kelasDelapan:pkd, kelasSembilan:pks }}) => {
	if (kelas == 7) { return lkt; }
	if (kelas == 8) { return lkd; }
	if (kelas == 9) { return lks; }
	if (kelas == 7) { return pkt; }
	if (kelas == 8) { return pkd; }
	if (kelas == 9) { return pks; }
};

const tugasSiswa = pembagianTugas(tugas);
const jamMulai = pembagianJamTugas(kelas);

const infoSiswa = `<li>Nama : ${name}</li> <li>Tugas : ${tugasSiswa}</li> <li>Jam Masuk : ${jamMulai}</li>`

let domDataSiswa = document.getElementById('dataSiswa').innerHTML = `<ul>${infoSiswa}</ul>`;